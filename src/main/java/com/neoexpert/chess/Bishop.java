package com.neoexpert.chess;
import java.util.*;

public final class Bishop extends Piece{
	public Bishop(boolean white){
		super(white?'♗':'♝',BISHOP, 3, white);
	}

	public Bishop(char icon, boolean white){
		super(icon, BISHOP,3,white);
	}
	public void calcPosibleMoves(int x, int y, Board b, ArrayList<Move> moves){
		Piece[][] board=b.board;
		Piece p;
		for(int _x=x+1,_y=y+1;_x<8&&_y<8;++_x,++_y){
			p=board[_x][_y];
			if(p==null)
				moves.add(new Move(x,y,_x,_y));
			else{
				if(p.white!=white)
					moves.add(new Move(x,y,_x,_y));
				break;
			}
		}
		for(int _x=x-1,_y=y+1;_x>=0&&_y<8;--_x,++_y){
			p=board[_x][_y];
			if(p==null)
				moves.add(new Move(x,y,_x,_y));
			else{
				if(p.white!=white)
					moves.add(new Move(x,y,_x,_y));
				break;
			}
		}
		for(int _x=x+1,_y=y-1;_x<8&&_y>=0;++_x,--_y){
			p=board[_x][_y];
			if(p==null)
				moves.add(new Move(x,y,_x,_y));
			else{
				if(p.white!=white)
					moves.add(new Move(x,y,_x,_y));
				break;
			}
		}
		for(int _x=x-1,_y=y-1;_x>=0&&_y>=0;--_x,--_y){
			p=board[_x][_y];
			if(p==null)
				moves.add(new Move(x,y,_x,_y));
			else{
				if(p.white!=white)
					moves.add(new Move(x,y,_x,_y));
				break;
			}
		}
	}
}
