package com.neoexpert.chess;
import java.util.*;

public class Board implements Constants{
	boolean whiteTurn=true;
	final Piece[][] board=new Piece[8][8];
	Pawn enPassant;
	public Board(){}
	public void reset(){
		board[A][0]=new Rook(WHITE);
		board[B][0]=new Knight(WHITE);
		board[C][0]=new Bishop(WHITE);
		board[D][0]=new Queen(WHITE);
		board[E][0]=new King(WHITE);
		board[F][0]=new Bishop(WHITE);
		board[G][0]=new Knight(WHITE);
		board[H][0]=new Rook(WHITE);
		board[A][1]=new WPawn();
		board[B][1]=new WPawn();
		board[C][1]=new WPawn();
		board[D][1]=new WPawn();
		board[E][1]=new WPawn();
		board[F][1]=new WPawn();
		board[G][1]=new WPawn();
		board[H][1]=new WPawn();

		board[A][7]=new Rook(BLACK);
		board[B][7]=new Knight(BLACK);
		board[C][7]=new Bishop(BLACK);
		board[D][7]=new Queen(BLACK);
		board[E][7]=new King(BLACK);
		board[F][7]=new Bishop(BLACK);
		board[G][7]=new Knight(BLACK);
		board[H][7]=new Rook(BLACK);
		board[A][6]=new BPawn();
		board[B][6]=new BPawn();
		board[C][6]=new BPawn();
		board[D][6]=new BPawn();
		board[E][6]=new BPawn();
		board[F][6]=new BPawn();
		board[G][6]=new BPawn();
		board[H][6]=new BPawn();
		evaluate();
	}
	float value;
	float whiteValue;
	float blackValue;
	final MovesList whiteMoves=new MovesList();
	final MovesList blackMoves=new MovesList();
	protected class MovesList extends ArrayList<Move>{
		@Override
		public boolean  add(Move m){
			State s=new State(Board.this, m);
			if(whiteTurn){
				if(s.whiteCheck)
					return false;
			}
			else
				if(s.blackCheck)
					return false;
			return super.add(m);	
		}
	};


	public void check(){
		float w=0, b=0;
		for(int x=0;x<8;++x)
			for(int y=0;y<8;++y){
				Piece p=board[x][y];
				if(p!=null){
					if(p.white){
						if(p.type==KING)
							checkWhite(x, y);
						w+=p.value;
					}
					else{
						if(p.type==KING)
							checkBlack(x, y);
						b+=p.value;
					}
				}
			}
		whiteValue=w;
		blackValue=b;
	}
	public void evaluate(){
		whiteCheck=false;
		blackCheck=false;
		whiteMoves.clear();
		blackMoves.clear();
		float w=0, b=0;
		for(int x=0;x<8;++x)
			for(int y=0;y<8;++y){
				Piece p=board[x][y];
				if(p!=null){
					if(p.white){
						if(p.type==KING)
							checkWhite(x, y);
						w+=p.value;
						//if(whiteTurn)
						p.calcPosibleMoves(x,y,this,whiteMoves);
					}
					else{
						if(p.type==KING)
							checkBlack(x, y);
						b+=p.value;
						//if(!whiteTurn)
							p.calcPosibleMoves(x,y,this,blackMoves);
					}
				}
			}
		whiteValue=w+whiteMoves.size()/2f;
		blackValue=b+blackMoves.size()/2f;
		value=whiteValue-blackValue;
	}
	public boolean whiteTurn(){
		return whiteTurn;
	}

	public void aiMove(){
		State s=new State(this);
		s.simulate(3);
		s.sort();
		if(s.children.length==0)
			return;
		int len=Math.min(5,s.children.length);
		for(int i=0;i<len;++i){
			if(whiteTurn)
				System.out.println(s.children[i].avg);
			else
				System.out.println(s.children[i].avg);
		}
		Move best=s.children[0].m;
		performMove(best);
		evaluate();
	}

	public void randomMove(){
		Random r=new Random();
		if(whiteTurn){
			if(!whiteMoves.isEmpty()){
				performMove(whiteMoves.get(r.nextInt(whiteMoves.size())));
				evaluate();
				return;
			}
		}
		else
			if(!blackMoves.isEmpty()){
				performMove(blackMoves.get(r.nextInt(blackMoves.size())));
				evaluate();
				return;
			}
	}

	public void performMove(String move){
		int x1=-1, y1=-1, x2=-1, y2=-1;
		int len=move.length();
		int i=0;
		for(;i<len;++i){
			char c=move.charAt(i);
			if(c>=0x61&&c<0x69){
				x1=c-0x61;
				break;
			}
		}
		if(i>=len)
			throw new RuntimeException();
		for(;i<len;++i){
			char c=move.charAt(i);
			if(c>=0x31&&c<0x39){
				y1=c-0x31;
				break;
			}
		}
		if(i>=len)
			throw new RuntimeException();
		for(;i<len;++i){
			char c=move.charAt(i);
			if(c>=0x61&&c<0x69){
				x2=c-0x61;
				break;
			}
		}
		if(i>=len)
			throw new RuntimeException();
		for(;i<len;++i){
			char c=move.charAt(i);
			if(c>=0x31&&c<0x39){
				y2=c-0x31;
				break;
			}
		}
		if(x1>=0&&y1>=0&&x2>=0&&y2>=0)
			performCheckedMove(new Move(x1,y1,x2,y2));
			
		else throw new RuntimeException();
	}

	public void performCheckedMove(Move m){
		MovesList posibleMoves;
		if(whiteTurn)
			posibleMoves=whiteMoves;
		else
			posibleMoves=blackMoves;
		System.out.println(m);
		Piece p=board[m.x1][m.y1];
		if(p==null)
			throw new RuntimeException("no piece at ");
		if(p.white!=whiteTurn)
			throw new RuntimeException("wrong piece");
		if(!posibleMoves.contains(m))
			throw new RuntimeException("wrong move");
		State s=new State(this, m);
		if(whiteTurn){
			if(s.whiteCheck)
				throw new RuntimeException("white check");
		}
		else
			if(s.blackCheck)
				throw new RuntimeException("black check");
		performMove(m);
		evaluate();
	}

	public void performMove(Move m){
		Piece p=board[m.x1][m.y1];
		board[m.x2][m.y2]=p;
		board[m.x1][m.y1]=null;
		whiteTurn=!whiteTurn;
		enPassant=null;
		p.afterMovePerformed(this, m);
	}

	public State createState(Move m){
		return new State(this, m);
	}

	boolean whiteCheck=false;
	boolean blackCheck=false;
	void checkWhite(int x, int y){
		Piece[][] board=this.board;
		Piece p;
		if(check(x,y,true)){
			whiteCheck=true;
			return;
		}
		if(x>0&&y<7){
			p=board[x-1][y+1];
			if(p!=null)
				if(!p.white&&p.type==PAWN){
					whiteCheck=true;
					return;
				}
		}
		if(x<7&&y<7){
			p=board[x+1][y+1];
			if(p!=null)
				if(!p.white&&p.type==PAWN){
					whiteCheck=true;
					return;
				}
		}
	}

	boolean check(int x, int y, boolean white){
		Piece[][] board=this.board;
		Piece p;
		for(int _x=x+1;_x<8;++_x){
			p=board[_x][y];
			if(p!=null){
				if(p.white==white)break;
				if(p.type==QUEEN||p.type==ROOK){
					return true;
				}
				break;
			}
		}
		for(int _x=x-1;_x>=0;--_x){
			p=board[_x][y];
			if(p!=null){
				if(p.white==white)break;
				if(p.type==QUEEN||p.type==ROOK){
					return true;
				}
				break;
			}
		}
		for(int _y=y+1;_y<8;++_y){
			p=board[x][_y];
			if(p!=null){
				if(p.white==white)break;
				if(p.type==QUEEN||p.type==ROOK){
					return true;
				}
				break;
			}
		}
		for(int _y=y-1;_y>=0;--_y){
			p=board[x][_y];
			if(p!=null){
				if(p.white==white)break;
				if(p.type==QUEEN||p.type==ROOK){
					return true;
				}
				break;
			}
		}
		for(int _x=x+1,_y=y+1;_x<8&&_y<8;++_x,++_y){
			p=board[_x][_y];
			if(p!=null){
				if(p.white==white)break;
				if(p.type==QUEEN||p.type==BISHOP){
					return true;
				}
				break;
			}
		}
		for(int _x=x-1,_y=y+1;_x>=0&&_y<8;--_x,++_y){
			p=board[_x][_y];
			if(p!=null){
				if(p.white==white)break;
				if(p.type==QUEEN||p.type==BISHOP){
					return true;
				}
				break;
			}
		}
		for(int _x=x+1,_y=y-1;_x<8&&_y>=0;++_x,--_y){
			p=board[_x][_y];
			if(p!=null){
				if(p.white==white)break;
				if(p.type==QUEEN||p.type==BISHOP){
					return true;
				}
				break;
			}
		}
		for(int _x=x-1,_y=y-1;_x>=0&&_y>=0;--_x,--_y){
			p=board[_x][_y];
			if(p!=null){
				if(p.white==white)break;
				if(p.type==QUEEN||p.type==BISHOP){
					return true;
				}
				break;
			}
		}
		if(x+2<8&&y+1<8){
			p=board[x+2][y+1];
			if(p!=null&&p.type==KNIGHT&&p.white!=white)
				return true;
		}
		if(x-2>=0&&y+1<8){
			p=board[x-2][y+1];
			if(p!=null&&p.type==KNIGHT&&p.white!=white)
				return true;
		}
		if(x+2<8&&y-1>=0){
			p=board[x+2][y-1];
			if(p!=null&&p.type==KNIGHT&&p.white!=white)
				return true;
		}
		if(x-2>=0&&y-1>=0){
			p=board[x-2][y-1];
			if(p!=null&&p.type==KNIGHT&&p.white!=white)
				return true;
		}

		if(x+1<8&&y+2<8){
			p=board[x+1][y+2];
			if(p!=null&&p.type==KNIGHT&&p.white!=white)
				return true;
		}
		if(x-1>=0&&y+2<8){
			p=board[x-1][y+2];
			if(p!=null&&p.type==KNIGHT&&p.white!=white)
				return true;
		}
		if(x+1<8&&y-2>=0){
			p=board[x+1][y-2];
			if(p!=null&&p.type==KNIGHT&&p.white!=white)
				return true;
		}
		if(x-1>=0&&y-2>=0){
			p=board[x-1][y-2];
			if(p!=null&&p.type==KNIGHT&&p.white!=white)
				return true;
		}
		//check for enemy King
		for(int _x=x-1;_x<=x+1;++_x)
			for(int _y=y-1;_y<=y+1;++_y)
				if(_x>=0&&_x<8&&_y>=0&&_y<8){
					if(_x==x&&_y==y)continue;
					p=board[_x][_y];
					if(p!=null&&p.type==KING)
						return true;
				}

		return false;	
	}
	void checkBlack(int x, int y){
		Piece[][] board=this.board;
		Piece p;
		if(check(x,y,false)){
			blackCheck=true;
			return;
		}
		if(x>0&&y>0){
			p=board[x-1][y-1];
			if(p!=null)
				if(p.white&&p.type==PAWN){
					blackCheck=true;
					return;
				}
		}
		if(x<7&&y>0){
			p=board[x+1][y-1];
			if(p!=null)
				if(p.white&&p.type==PAWN){
					blackCheck=true;
					return;
				}
		}
	
	}
	
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("white: ")
			.append(whiteValue)
			.append("\n")
			.append("black: ")
			.append(blackValue)
			.append("\n");
		/*
		sb.append("posibleMoves: ")
			.append(posibleMoves.size())
		.append("\n");
		for(Move m:posibleMoves)
			sb.append(m)
				.append("\n");
		sb.append("\n");
		*/
		Piece[][] board=this.board;
		for(int y=7;y>=0;--y){
			for(int x=0;x<8;++x){
				Piece p=board[x][y];
				if(p==null)
					sb.append(" ");
				else
					sb.append(p.icon);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
