package com.neoexpert.chess;
public interface Constants{
	boolean WHITE=true;
	boolean BLACK=false;
	int A=0;
	int B=1;
	int C=2;
	int D=3;
	int E=4;
	int F=5;
	int G=6;
	int H=7;
	int _1=0;
	int _2=1;
	int _3=2;
	int _4=3;
	int _5=4;
	int _6=5;
	int _7=6;
	int _8=7;
	byte KING=0;
	byte QUEEN=1;
	byte ROOK=2;
	byte BISHOP=3;
	byte KNIGHT=4;
	byte PAWN=5;
}
