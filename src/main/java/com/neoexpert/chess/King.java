package com.neoexpert.chess;
import java.util.*;

public final class King extends Piece{
	public King(boolean white){
		super(white?'♔':'♚',KING, 0, white);
	}
	public King(char icon, boolean white, boolean moved){
		super(icon, KING, 0, white);
		this.moved=moved;
	}

	public void calcPosibleMoves(int x, int y, Board b, ArrayList<Move> moves){
		Piece[][] board=b.board;
		for(int _x=x-1;_x<=x+1;++_x)
			for(int _y=y-1;_y<=y+1;++_y)
				if(_x>=0&&_x<8&&_y>=0&&_y<8){
					if(_x==x&&_y==y)continue;
					if(board[_x][_y]==null)
						moves.add(new Move(x,y,_x,_y));
					else if(board[_x][_y].white!=white)
						moves.add(new Move(x,y,_x,_y));
				}
		if(moved)return;
		if(white){
			if(b.whiteCheck)
				return;
			whiteCastling(x,y,b,moves);
		}
		else{
			if(b.blackCheck)
				return;
			blackCastling(x,y,b,moves);
		}
	}
	public void whiteCastling(int x, int y, Board b, ArrayList<Move> moves){
		Piece[][] board=b.board;
		if(	board[F][_1]==null&&
				board[G][_1]==null){
					Piece p=board[H][_1];
					if(p!=null&&p.type==ROOK&&!p.moved){
						if(
								!new State(b, new Move(x,y,F,_1)).whiteCheck&&
								!new State(b, new Move(x,y,G,_1)).whiteCheck
								){
									moves.add(new Move(x,y,G,_1));	
								}	
					}
				}
		if(	board[D][_1]==null&&
				board[C][_1]==null&&
				board[B][_1]==null){
					Piece p=board[A][_1];
					if(p!=null&&p.type==ROOK&&!p.moved){
						if(
								!new State(b, new Move(x,y,D,_1)).whiteCheck&&
								!new State(b, new Move(x,y,C,_1)).whiteCheck&&
								!new State(b, new Move(x,y,B,_1)).whiteCheck
								){
									moves.add(new Move(x,y,B,_1));	
								}	
					
					}
				}
	}
	public void blackCastling(int x, int y, Board b, ArrayList<Move> moves){
		Piece[][] board=b.board;
		if(	board[F][_8]==null&&
				board[G][_8]==null){
					Piece p=board[H][_1];
					if(p!=null&&p.type==ROOK&&!p.moved){
						if(
								!new State(b, new Move(x,y,F,_1)).blackCheck&&
								!new State(b, new Move(x,y,G,_1)).blackCheck
								){
									moves.add(new Move(x,y,G,_1));	
								}	
					}
				}
		if(	board[D][_8]==null&&
				board[C][_8]==null&&
				board[B][_8]==null){
					Piece p=board[A][_1];
					if(p!=null&&p.type==ROOK&&!p.moved){
						if(
								!new State(b, new Move(x,y,D,_1)).blackCheck&&
								!new State(b, new Move(x,y,C,_1)).blackCheck&&
								!new State(b, new Move(x,y,B,_1)).blackCheck
								){
									moves.add(new Move(x,y,B,_1));	
								}	
					
					}
				}
	}
	public void afterMovePerformed(Board b, Move m){
		if(moved)return;
		moved=true;
		Piece[][] board=b.board;
		if(m.x1==E&&m.x2==G){
			board[F][m.y2]=board[H][m.y2];
			board[H][m.y2]=null;
			return;
		}
		if(m.x1==E&&m.x2==B){
			board[C][m.y2]=board[A][m.y2];
			board[A][m.y2]=null;
		}

	}
	public Piece clone(){
		return new King(icon, white, moved);
	}
}
