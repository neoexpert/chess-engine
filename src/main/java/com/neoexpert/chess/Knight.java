package com.neoexpert.chess;
import java.util.*;

public final class Knight extends Piece{
	public Knight(boolean white){
		super(white?'♘':'♞', KNIGHT, 3, white);
	}

	public void calcPosibleMoves(final int x, final int y, Board b, ArrayList<Move> moves){
		Piece[][] board=b.board;
		Piece p;
		if(x+2<8&&y+1<8){
			p=board[x+2][y+1];
			if(p==null||(p!=null&&p.white!=white))
				moves.add(new Move(x,y,x+2,y+1));
		}
		if(x-2>=0&&y+1<8){
			p=board[x-2][y+1];
			if(p==null||(p!=null&&p.white!=white))
				moves.add(new Move(x,y,x-2,y+1));
		}
		if(x+2<8&&y-1>=0){
			p=board[x+2][y-1];
			if(p==null||(p!=null&&p.white!=white))
			moves.add(new Move(x,y,x+2,y-1));
		}
		if(x-2>=0&&y-1>=0){
			p=board[x-2][y-1];
			if(p==null||(p!=null&&p.white!=white))
			moves.add(new Move(x,y,x-2,y-1));
		}

		if(x+1<8&&y+2<8){
			p=board[x+1][y+2];
			if(p==null||(p!=null&&p.white!=white))
			moves.add(new Move(x,y,x+1,y+2));
		}
		if(x-1>=0&&y+2<8){
			p=board[x-1][y+2];
			if(p==null||(p!=null&&p.white!=white))
			moves.add(new Move(x,y,x-1,y+2));
		}
		if(x+1<8&&y-2>=0){
			p=board[x+1][y-2];
			if(p==null||(p!=null&&p.white!=white))
			moves.add(new Move(x,y,x+1,y-2));
		}
		if(x-1>=0&&y-2>=0){
			p=board[x-1][y-2];
			if(p==null||(p!=null&&p.white!=white))
			moves.add(new Move(x,y,x-1,y-2));
		}
	}
}
