package com.neoexpert.chess;

import java.util.Scanner;

public class Main{
	public static void main(String ... args){
		Board b=new Board();
		b.reset();
		Scanner s=new Scanner(System.in);
		while(true){
			System.out.println(b);	
			if(b.whiteTurn())
				System.out.println("white turn: ");
			else
				System.out.println("black turn: ");
			String input = s.nextLine();
			switch(input){
				case "e":
					b.evaluate();
					continue;
				case "":
					b.aiMove();
					continue;
			}
			try{
				b.performMove(input);
			}
			catch(RuntimeException e){
				System.out.println("can not perform move: "+e.getMessage());
			}
		}
	}
}
