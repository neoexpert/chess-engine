package com.neoexpert.chess;

public class Move{
	public Move(int x1, int y1, int x2, int y2){
		this.x1=x1;
		this.y1=y1;
		this.x2=x2;
		this.y2=y2;
	}
	int x1,y1;
	int x2,y2;

	public char xToChar(int x){
		return (char)(x+(65+32));
	}
	public boolean equals(Object o){
		if(!(o instanceof Move))return false;
		Move m=(Move)o;
		return m.x1==x1&&m.y1==y1&&m.x2==x2&&m.y2==y2;
	}

	public String toString(){
		return 
			new StringBuilder()
					.append(xToChar(x1))
					.append(y1+1)
					.append(" -> ")
					.append(xToChar(x2))
					.append(y2+1)
					.toString();
	}
}

