package com.neoexpert.chess;
import java.util.*;

public abstract class Pawn extends Piece{
	public Pawn(char icon, boolean white){
		super(icon, PAWN, 1, white);
	}
}
