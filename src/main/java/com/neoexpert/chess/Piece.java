package com.neoexpert.chess;
import java.util.*;

public abstract class Piece implements Constants{
	public final float value;
	public final boolean white;
	public final byte type;
	public final char icon;
	boolean moved=false;
	public Piece(char icon, byte type, float value, boolean white){
		this.value=value;
		this.type=type;
		this.white=white;
		this.icon=icon;
	}
	public abstract void calcPosibleMoves(int x, int y, Board b, ArrayList<Move> moves);
	public void afterMovePerformed(Board b, Move m){}
	public Piece clone(){
		return this;
	}
}
