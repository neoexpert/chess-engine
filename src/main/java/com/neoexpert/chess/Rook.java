package com.neoexpert.chess;
import java.util.*;

public final class Rook extends Piece{
	public Rook(boolean white){
		super(white?'♖':'♜', ROOK, 5, white);
	}
	public Rook(char icon, boolean white, boolean moved){
		super(icon, ROOK, 5, white);
		this.moved=moved;
	}
	public void calcPosibleMoves(final int x, final int y, Board b, ArrayList<Move> moves){
		Piece[][] board=b.board;
		Piece p;
		for(int _x=x+1;_x<8;++_x){
			p=board[_x][y];
			if(p==null)
				moves.add(new Move(x,y,_x,y));
			else{
				if(p.white!=white)
					moves.add(new Move(x,y,_x,y));
				break;
			}
		}
		for(int _x=x-1;_x>=0;--_x){
			p=board[_x][y];
			if(p==null)
				moves.add(new Move(x,y,_x,y));
			else{
				if(p.white!=white)
					moves.add(new Move(x,y,_x,y));
				break;
			}
		}
		for(int _y=y+1;_y<8;++_y){
			p=board[x][_y];
			if(p==null)
				moves.add(new Move(x,y,x,_y));
			else{
				if(p.white!=white)
					moves.add(new Move(x,y,x,_y));
				break;
			}
		}
		for(int _y=y-1;_y>=0;--_y){
			p=board[x][_y];
			if(p==null)
				moves.add(new Move(x,y,x,_y));
			else{
				if(p.white!=white)
					moves.add(new Move(x,y,x,_y));
				break;
			}
		}
	}
	public void afterMovePerformed(Board b, Move m){
		moved=true;
	}
	public Piece clone(){
		return new Rook(icon, white, moved);
	}
}
