package com.neoexpert.chess;
import java.util.*;

public class State extends Board implements Constants{
	protected State parent;
	protected State[] children;
	Move m;
	public State(Board b, Move m){
		this(b);
		performMove(m);
		check();
		this.m=m;
	}
	public State(Board b){
		Piece[][] b1=board;
		Piece[][] b2=b.board;
		for(int x=0;x<8;++x)
			for(int y=0;y<8;++y){
				Piece p=b2[x][y];
				if(p==null)continue;
				b1[x][y]=p.clone();
			}
		this.whiteTurn=b.whiteTurn;
		this.whiteMoves.addAll(b.whiteMoves);
		this.blackMoves.addAll(b.blackMoves);
	}
	float avg=0f;
	float avgWhite=0f;
	float avgBlack=0f;
	public void simulate(int depth){
		if(depth<=0)
			return;
		MovesList posibleMoves;
		if(whiteTurn)
			posibleMoves=whiteMoves;
		else
			posibleMoves=blackMoves;
		int len=posibleMoves.size();
		children=new State[len];
		for(int i=0;i<len;++i){
			State s=new State(this, posibleMoves.get(i));
			s.evaluate();
			s.simulate(depth-1);
			children[i]=s;
			avgWhite+=s.whiteValue/len;
			avgBlack+=s.blackValue/len;
			avg+=s.value/len;
		}
	}

	public void sort(){
		if(whiteTurn)
			Arrays.sort(children, whiteComparator);
		else
			Arrays.sort(children, blackComparator);
	}

	Comparator<State> whiteComparator=new Comparator<State>(){
		public int compare(State s1, State s2){
			if(s1.avg==s2.avg)
				return 0;
			else if(s1.avg>s2.avg)
				return -1;
			else
				return 1;
		}
	};
	Comparator<State> blackComparator=new Comparator<State>(){
		public int compare(State s1, State s2){
			if(s1.avg==s2.avg)
				return 0;
			else if(s1.avg>s2.avg)
				return 1;
			else
				return -1;
		}
	};
}
