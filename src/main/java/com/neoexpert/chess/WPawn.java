package com.neoexpert.chess;
import java.util.*;

public final class WPawn extends Pawn{
	public WPawn(){
		super('♙', true);
	}

	public void calcPosibleMoves(int x, int y, Board b, ArrayList<Move> moves){
		Piece[][] board=b.board;
		Piece p;
		//ahead
		if(board[x][y+1]==null){
			moves.add(new Move(x,y,x,y+1));
			//double
			if(y==1)
				if(board[x][y+2]==null)
					moves.add(new Move(x,y,x,y+2));
		}
		//ahead right
		if(x+1<8){
			p=board[x+1][y+1];
			if(p!=null&&!p.white)
				moves.add(new Move(x,y,x+1,y+1));
		}

		//ahead left
		if(x-1>=0){
			p=board[x-1][y+1];
			if(p!=null&&!p.white)
				moves.add(new Move(x,y,x-1,y+1));
		}
		//en passant
		if(y==_5&&b.enPassant!=null){
			if(board[x+1][y]==b.enPassant){
				moves.add(new Move(x,y,x+1,y+1));
				return;	
			}
			if(board[x-1][y]==b.enPassant){
				moves.add(new Move(x,y,x-1,y+1));
				return;	
			}
		}
	}

	@Override
	public void afterMovePerformed(Board b, Move m){
		if(m.y2==_8){
			b.board[m.x2][m.y2]=new Queen(WHITE);
		}
		//en passant
		if(m.y1==_5&&m.x1!=m.x2){
			if(b.board[m.x2][m.y2]==null){
				b.board[m.x2][m.y2-1]=null;
			}
		}
	}
}
